# README

- Date created: 20180704

## How to submit your code assignment

1. Fork the project from GitLab.
2. Make your forked project to **private** from GitLab -> Settings -> General -> Permissions -> Project visibility.
3. Add member of **singtel_shijian** in **Select members to invite**, and choose the role permission to be **Developer** from GitLab -> Settings -> Members.
4. Complete your code and documentation in the forked project.
5. Upload your resume or CV in the root folder of the forked project.
6. Create a new plain text file in the root folder of the forked project. And make the file name in the format of "your employer + your full name", e.g. **Your Employer Name + John Doe**.
7. Update the forked project description in GitLab to "your employer + full name", e.g. **Your Employer Name + John Doe** from GitLab -> Settings -> General -> General project -> Project description.  
8. Notify the code assignment is done.

## Code assignment details

Imagine you are the software engineer who is going to design and develop a small application. The application is to provide 2 Restful APIs for Book.

The application has 1 domain model named "**Book**" The domain model is Book, which has below minimal information,

- Attribute to act as the unique identifier of a Book
- Attribute to show what the book is or what the book is about. This attribute is able to be understood by the human and has some meaning, for example, it is not a random number.

You can make the decision about the names and data type of above attributes.

Here is technical requirement for the application.

- Use **Spring framework**, you can consider to use Spring MVC (version >= 4.3.13.RELEASE) or preferably Spring Boot (version >= 1.5.9.RELEASE). If you choose to use Spring MVC, you need to provide the embedded web server to run your application alone without external web server (hints: for example, by using Jetty or Tomcat Maven plugin).
- The building tool is Maven 3 or preferably Gradle 4.
- 1 API with HTTP method POST to handle the new entry of Book or update the existing Book information. The HTTP request and response are in JSON format.
- 1 API with HTTP method GET to get the Book information by its unique identifier. The HTTP response is in JSON format.
- You need to have error handling and provide the meaningful error message and status.
- There is no database or external file storage required in this application. You shall not have the function with any kinds of database or storage, e.g. MySQL, H2, Postgresql, or MongoDB.
- Your code shall not be broke in a very simple test.
- It is no need to have complex code structure of the application, but the code shall be clear and able to be understood. The code also shall follow the Java coding standard.
- Consider having appropriate documentation in README.md and Javadoc.
- In the README.md, you need to complete the steps and commands section to show how to run your application, please keep it short and get to the point.
- In the README.md, you need to complete the test commands section by using `curl` to simulate the request for your 2 APIs. You need to provide the payload of the response.
- You need to provide the unit test.
- [**Bonus Question**] You can provide 2 or 3 more domain models to associate with the Book.

Please keep your implementation simple. It is no need to over design and over development. But the code shall be able to show your basic understanding of the design and development knowledge with MVC and Spring framework.

You can assume your implementation can be completed in 8 hours. Normally, it could be completed in 2 to 6 hours.

## Steps and commands to run the application

To be completed by the candidate.

Configuring the db configurations in application.properties
I am using my sql db. the details I used as followed.

I created table as my_book with the field names as follows
b_id varchar2(15),
b_name varchar2(25),
b_desc varchar2(25),
b_price decimal(10)


spring.datasource.name=mydatabase
spring.datasource.url=jdbc:mysql://localhost:3306/test_schema
spring.datasource.username=root
spring.datasource.password=root

We can test this application via postman
Test 1: 	
url: localhost://8080/books 
select put method/post method 
choose Body tab
select raw (radio button) and select from text JSON application from drop-down box
enter some data some thing like 
{
    "b_id": "1A",
    "b_name": "Java",
    "b_desc": "V1",
    "b_price": 50
}

hit send
The data will store in the database 


Test 2: 	
url: localhost://8080/books/1A 
select get method 
hit send
system will sends the request and fetch data from data from database.

## API testing commands

To be completed by the candidate.

Test 1: 
Postman testing : insert/update data 
url: localhost://8080/books 
follow the above steps and test the database for confirmation.

Test 2: 
Postman testing : fetching data giving boodk Id
url: localhost://8080/books/1A 
follow the above steps and test the database for confirmation.

Test 3:
Junit  : insert/updating data
I have created a java class for testing this method "BookAPITest.java" ==> createBook()
just right click on the class and run as java application.
It will run and shows the results.
Here I hard coded, just need to change the model values and run.


Test 4:
Junit  : fetching data
I have created a java class for testing this method "BookAPITest.java" ==>getBook()
just right click on the class and run as java application.
It will run and shows the results.
Here I hard coded, just need to change the model values and run.
